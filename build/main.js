var boids;
var canvas, ctx;
var wWidth = window.innerWidth - 2;
var wHeight = window.innerHeight - 2;
var cWidth;
var cHeight;

var mouseX = 0;
var mouseY = 0;

var previousStep = 0;
var quadTree = null;

function maxBoids()
{
    let a = Math.max(wWidth, wHeight)
    let b = Math.min(wWidth, wHeight)
    return Math.min(2000, Math.ceil((a / b) * 562.5));
}

document.addEventListener('mousemove', event =>
{
    mouseX = event.clientX;
    mouseY = event.clientY;
});

function init()
{
    quadTree = new QuadTree(wWidth, wHeight);
    boids = [];

    canvas = document.getElementById('canvas');
    if (canvas.getContext)
        ctx = canvas.getContext('2d');

    loop();
}

function loop()
{
    if (boids.length < maxBoids() - 1)
    {
        boids.push(new boid(Math.random() * wWidth, Math.random() * wHeight, quadTree))
    }
    else if (boids.length > maxBoids() + 1)
    {
        quadTree.remove(boids[boids.length - 1]);
        boids.splice(boids.length - 1, 1);
    }

    previousStep = previousStep || 0;
    let stepTime = Date.now();
    let stepTimeCurr = (stepTime - previousStep) * 0.001;
    stepTimeCurr = Math.min(stepTimeCurr, 0.1);
    if (isNaN(stepTimeCurr))
        stepTimeCurr = 0;

    canvas.width = window.innerWidth - 2;
    canvas.height = window.innerHeight - 2;

    wWidth = canvas.width;
    wHeight = canvas.height;

    cWidth = wWidth / 2;
    cHeight = wHeight / 2;

    ctx.fillStyle = "#000000";
    //ctx.globalAlpha = 0.5;
    ctx.fillRect(0, 0, wWidth, wHeight);
    ctx.fillStyle = "#ffffff";
    //ctx.strokeStyle = "#ffffff";

    ctx.globalAlpha = 1;

    boids.forEach(boid =>
    {
        boid.step(stepTimeCurr);
        boid.draw(ctx);
    });

    quadTree.update(stepTimeCurr);

    previousStep = stepTime;

    requestAnimationFrame(loop);
}

class boid
{
    position = new vector2();
    velocity = new vector2();

    get size()
    {
        let w = wWidth / 1920;
        let h = wHeight / 1080;
        return 1.5 * Math.min(w, h);
    }

    tailScale = 2.5;

    get maxRange() { return 35 * this.size }

    get minspeed()
    {
        return 1 * (this.size / 1.5);
    }

    get targetSpeed()
    {
        return 2.5 * (this.size / 1.5)
    }

    get maxSpeed()
    {
        return 4.5 * (this.size / 1.5)
    }

    r = Math.random() * 255;
    g = Math.random() * 255;
    b = Math.random() * 255;

    get magnetStrength() { return 0.01 * this.size; }

    quadTree = null;

    aR = 0;
    aG = 0;
    aB = 0;

    constructor(x, y, quadTree)
    {
        this.position.x = x;
        this.position.y = y;

        this.velocity.x = this.targetSpeed * (Math.random() - 0.5);
        this.velocity.y = this.targetSpeed * (Math.random() - 0.5);

        let rand = Math.random();

        this.r = 255;
        this.g = 0;
        this.b = 0;
        this.quadTree = quadTree;

        if (rand >= 0.36)
        {
            this.r = 0;
            this.b = 0;
            this.g = 255;
        }
        if (rand >= 0.66)
        {
            this.r = 0;
            this.b = 255;
            this.g = 0;
        }
    }

    checkQuadtree()
    {
        if (this.node)
        {
            if (this.position.x < this.node.x1
                || this.position.x > this.node.x2
                || this.position.y < this.node.y1
                || this.position.y > this.node.y2)
            {
                this.quadTree.remove(this);
                this.quadTree.add(this);
            }
        }
        else
        {
            this.quadTree.add(this);
        }
    }

    step(deltaTime)
    {
        this.getDesiredAngle();

        if (this.velocity.getLength() < this.minspeed)
            this.velocity.normalize().multiply(this.minspeed);

        if (this.velocity.getLength() > this.maxSpeed)
            this.velocity.normalize().multiply(this.maxSpeed);

        this.position.x += this.velocity.x * deltaTime * 50;
        this.position.y += this.velocity.y * deltaTime * 50;

        if (this.position.x + this.velocity.x < 0 || this.position.x + this.velocity.x > wWidth)
        {
            this.velocity.x *= -1;
            this.position.x += this.velocity.x * 2;
        }

        if (this.position.y + this.velocity.y < 0 || this.position.y + this.velocity.y > wHeight)
        {
            this.velocity.y *= -1;
            this.position.y += this.velocity.y * 2;
        }

        this.position.x = Math.max(Math.min(this.position.x, wWidth - 1), 1)
        this.position.y = Math.max(Math.min(this.position.y, wHeight - 1), 1)

        this.checkQuadtree();
    }

    getDesiredAngle()
    {
        let total = 0;
        let averagePosition = new vector2();
        let averageVelocity = new vector2();
        let closest = null;
        let closestD = 10000;

        this.quadTree.getObjectsInRange(this.position, this.maxRange).forEach(other =>
        {
            let dX = this.position.x - other.position.x;
            let dY = this.position.y - other.position.y;
            let distance = Math.sqrt(dX * dX + dY * dY);

            if (distance < this.maxRange && other != this)
            {
                total++;
                averageVelocity.x += other.velocity.x;
                averageVelocity.y += other.velocity.y;

                averagePosition.x += other.position.x;
                averagePosition.y += other.position.y;
                this.aR += other.r;
                this.aG += other.g;
                this.aB += other.b;

                if (distance < closestD)
                {
                    closestD = distance;
                    closest = other;
                }
            }
        });

        let dX = this.position.x - mouseX;
        let dY = this.position.y - mouseY;
        let mouseDistance = Math.sqrt(dX * dX + dY * dY);
        let maxMouseRange = (this.maxRange * 1.5);

        let awayFromMouse = new vector2();
        awayFromMouse.x = this.position.x - mouseX;
        awayFromMouse.y = this.position.y - mouseY;
        awayFromMouse.normalize().multiply(this.targetSpeed * 2);

        let courage = total >= 1 ? 0.9 / Math.min(total, 10) : 0.9;
        let panicStrength = Math.max((1 - Math.min(mouseDistance / maxMouseRange, 1)) * courage, 0);

        if (mouseDistance < maxMouseRange)
        {
            this.velocity = vector2.Lerp(this.velocity, awayFromMouse, Math.max(panicStrength - 0.01, 0));
        }
        else
        {
            panicStrength = 0;
        }

        if (total > 1)
        {
            averagePosition.x /= total;
            averagePosition.y /= total;
            averageVelocity.x /= total;
            averageVelocity.y /= total;
            this.aR /= total;
            this.aG /= total;
            this.aB /= total;

            let delta = new vector2();
            delta.x = this.position.x - averagePosition.x;
            delta.y = this.position.y - averagePosition.y;
            let distance = Math.sqrt(delta.x * delta.x + delta.y * delta.y);

            let awayFromBoid = new vector2();
            awayFromBoid.x = this.position.x - closest.position.x;
            awayFromBoid.y = this.position.y - closest.position.y;
            awayFromBoid.normalize().multiply(this.targetSpeed * 2);

            let toCenter = new vector2();
            toCenter.x = cWidth - this.position.x;
            toCenter.y = cHeight - this.position.y;
            let distToCenter = toCenter.getLength();
            toCenter.normalize().multiply(this.targetSpeed);

            delta.normalize();
            delta.multiply(this.targetSpeed);

            let towardsDelta = delta.clone().multiply(-1);
            let toAvg = vector2.Lerp(averageVelocity, towardsDelta, Math.abs(distance - this.maxRange / 2) / this.maxRange / 1.7)

            if (closestD < (this.maxRange / 8))
            {
                toAvg = vector2.Lerp(toAvg, awayFromBoid, (1 - Math.min(closestD / (this.maxRange / 2.4), 1)) * 0.42);
            }

            toAvg = vector2.Lerp(toAvg, toCenter, (distToCenter / (Math.min(wWidth, wHeight) / 2)) * 0.02)

            if (toAvg.getLength() < this.targetSpeed)
                toAvg.normalize().multiply(this.targetSpeed);

            this.velocity = vector2.Lerp(this.velocity, toAvg, this.magnetStrength);
        }
        else
        {
            this.aR = this.r;
            this.aG = this.g;
            this.aB = this.b;
        }

        this.aR = lerp(this.aR, Math.random() * 255, panicStrength * 20);
        this.aG = lerp(this.aG, Math.random() * 255, panicStrength * 20);
        this.aB = lerp(this.aB, Math.random() * 255, panicStrength * 20);
    }

    draw(ctx)
    {
        let forwardAngle = this.velocity.getAngle() + Math.PI;
        let rB = forwardAngle + 1;
        let lB = forwardAngle - 1;

        ctx.lineWidth = 2;
        ctx.fillStyle = "rgb(" + this.aR + ", " + this.aG + ", " + this.aB + ")";
        ctx.beginPath();
        let tailLength = this.velocity.getLength() * this.tailScale;
        ctx.moveTo(this.position.x + Math.sin(forwardAngle) * (this.size * tailLength), this.position.y + Math.cos(forwardAngle) * (this.size * tailLength));
        ctx.lineTo(this.position.x + Math.sin(rB) * this.size, this.position.y + Math.cos(rB) * this.size);
        ctx.lineTo(this.position.x + Math.sin(lB) * this.size, this.position.y + Math.cos(lB) * this.size);
        ctx.fill();

        ctx.beginPath();
        ctx.arc(this.position.x, this.position.y, this.size, 0, 2 * Math.PI);
        //ctx.stroke();
        ctx.fill();
    }
}

function lerp(a, b, f)
{
    return a + f * (b - a);
}

class vector2
{
    x = 0;
    y = 0;

    getAngle()
    {
        return (((360 + Math.round(180 * Math.atan2(this.y, -this.x) / Math.PI)) % 360) - 90) * 0.0174532925;
    }

    getLength()
    {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    normalize()
    {
        let length = this.getLength();
        this.x /= length;
        this.y /= length;
        return this;
    }

    distanceTo(otherVector)
    {
        let self = this.clone();
        self.x -= otherVector.x;
        self.y -= otherVector.y;
        return self.getLength();
    }

    multiply(n)
    {
        this.x *= n;
        this.y *= n;
        return this;
    }

    clone()
    {
        let newVector = new vector2();
        newVector.x = this.x;
        newVector.y = this.y;
        return newVector;
    }

    static Lerp(vA, vB, t)
    {
        let newVector = new vector2();
        newVector.x = vA.x + (vB.x - vA.x) * t;
        newVector.y = vA.y + (vB.y - vA.y) * t;
        return newVector;
    }
}

window.onload = init;

/**
 * Quadtree is a data-structure that contains a root node, and child nodes up to a defined depth. These nodes can be considered 'buckets' that can hold up to X objects.
 *  When that number is exceeded (and the maximum depth is not yet reached), the node will split up into 4 separate nodes 
 *                                                          (top left, top right, bottom left, bottom right), 
 *  dividing up the objects into the correct quadrant based on their location.
 * 
 */

class QuadTree
{
    bucketSize = 30;

    constructor(width, height)
    {
        this.root = new Node(0, 0, width, height, 0, null, "", this);

        this.maxDepth = 7;

        this.width = width;
        this.height = height;

        this.merge = [];
    }

    /**
     * Adds object to the quadtree. if the amount of objects in the target node is higher than 
     *  the permitted size, it divides up into 4 new nodes. Contents of this node is then divided up.
     * @param {GameObject} object 
     * @returns {void}
     */
    add(object)
    {
        let dirOfOob = this.outOfBounds(object);
        if (dirOfOob)
        {
            this.embiggen(dirOfOob);
        }

        if (!this.root.hasChildren)
        {
            this.root.add(object);
            object.node = this.root;

            return;
        }

        let node = this.findNodeFromRoot(object);

        node.add(object);
        object.node = node;
    }

    /**
     * Removes object from quadtree. If the total amount of objects held by siblings is lower than the maximum size,
     *  a cleanup phase is triggered to remove the objects from siblings, moved up to parent node 
     * @param {GameObject} object 
     * @returns {void}
     */

    remove(object)
    {
        if (!this.root.hasChildren)
        {
            this.root.remove(object);
            object.node = null;
            return;
        }

        let node = object.node;
        object.node = null;

        node.remove(object);
    }

    /**
     * Retrieves objects within range of a given position. Sorted by distance from position
     * 
     * @param {vector} position 
     * @param {number} range (radius)
     * @returns {GameObject[]} result
     */

    getObjectsInRange(position, range)
    {
        return this.getObjectsOfTypesInRange(position, range, null);
    }

    /**
     * Retrieves objects within range of a given position matching a type. Sorted by distance from position
     * 
     * @param {vector} position 
     * @param {number} range (radius)
     * @param {class[]} types 
     * @returns {GameObject[]} result
     */

    getObjectsOfTypesInRange(position, range, types)
    {
        let result = [];

        let nodes = [];

        nodes.push(this.root);

        while (nodes.length > 0)
        {
            let node = nodes.pop();

            if (this.isInRange(position, range, node))
            {
                if (!node.hasChildren)
                {
                    let filtered = node.getObjects()
                        .filter(obj => obj.position.distanceTo(position) < range && obj.position.distanceTo(position) > 0);

                    if (types != null)
                    {
                        filtered = filtered.filter(obj => types.some(t => obj instanceof t));
                    }

                    result.push(...filtered);
                }
                else
                {
                    nodes.push(...node.getChildren());
                }
            }
        }

        return result.sort((a, b) => a.position.distanceTo(position) - b.position.distanceTo(position));
    }

    isInRange(vector, range, node)
    {
        var points =
            [
                vector.x - range,
                vector.x + range,
                vector.y - range,
                vector.y + range
            ];

        if (points[1] >= node.x1 && points[0] <= node.x2)
        {
            if (points[3] >= node.y1 && points[2] <= node.y2)
            {
                return true;
            }
        }
        return false;
    }

    embiggen(dirOfOob)
    {
        if (dirOfOob == "L")
        {
            this.root = this.root.createParent("bottomright");
        }
        else if (dirOfOob == "R")
        {
            this.root = this.root.createParent("topleft");
        }
        else if (dirOfOob == "U")
        {
            this.root = this.root.createParent("bottomleft");
        }
        else if (dirOfOob == "D")
        {
            this.root = this.root.createParent("topleft");
        }

    }

    traverseTo(path)
    {
        let node = this.root;

        for (let i = 0, len = path.length; i < len; i++)
        {
            if (!node.hasChildren)
                return node;

            node = node.getChildren()[path[i]];
        }

        return node;
    }

    findNodeFromRoot(object)
    {
        return this.findNodeFromRootAtPosition(object.position)
    }

    findNodeFromRootAtPosition(position)
    {
        if (!this.root.hasChildren)
        {
            return this.root;
        }

        let node = this.root.findChildNode(position);

        while (node.hasChildren)
        {
            node = node.findChildNode(position);
        }

        return node;
    }

    countObjectsInChild(parentNode)
    {
        if (!parentnode.hasChildren)
        {
            return 0;
        }

        return parentNode.topleft.size() + parentNode.topright.size()
            + parentNode.bottomleft.size() + parentNode.bottomright.size();
    }

    show(ctx)
    {
        this.showAll(this.root, ctx);
    }

    showAll(node, ctx)
    {
        node.show(ctx);

        if (node.hasChildren)
        {
            node.getChildren().forEach((element) => this.showAll(element, ctx));
        }
    }

    update(deltaTime)
    {
        this.root.update(deltaTime);
    }

    outOfBounds(object)
    {
        let pos = object.position;

        if (pos.x < this.root.x1)
        {
            return "L";
        }

        if (pos.x > this.root.x2 + this.root.x1)
        {
            return "R";
        }

        if (pos.y < this.root.y1)
        {
            return "U";
        }

        if (pos.y > this.root.y2 + this.root.y1)
        {
            return "D";
        }

        return false;
    }
}

class Node
{
    get canBeMerged()
    {
        return !this.hasChildren && this.parent && this.parent.recursiveCount < this.quadTree.bucketSize;
    }

    get shouldSplit()
    {
        return !this.hasChildren && this.recursiveCount > this.quadTree.bucketSize;
    }

    get hasChildren()
    {
        return this.bottomright != null; //assume the rest is there too
    }

    constructor(x1, y1, x2, y2, depth, parent, name, quadTree)
    {
        this.name = name;

        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;

        this.objects = [];

        this.topleft = null;
        this.topright = null;
        this.bottomleft = null;
        this.bottomright = null;

        this.parent = parent;

        this.depth = depth;
        this.recursiveCount = 0;

        this.lengthX = this.x2 - this.x1;
        this.lengthY = this.y2 - this.y1;

        this.halfLengthX = this.lengthX / 2;
        this.halfLengthY = this.lengthY / 2;

        this.markedForCleanup = false;
        this.timer = 0;

        this.quadTree = quadTree;
    }

    addCount()
    {
        this.recursiveCount++;

        this.parent && this.parent.addCount();

        if (this.shouldSplit)
        {
            this.divide();
        }
    }

    removeCount()
    {
        this.recursiveCount--;

        this.parent && this.parent.removeCount();

        if (this.hasChildren)
        {
            let merge = true;
            let children = this.getChildren()
            for (let i = 0, l = children.length; i < l; i++)
            {
                if (!children[i].canBeMerged)
                    merge = false;
            }

            if (merge)
            {
                this.merge();
            }
        }
    }

    remove(obj)
    {
        if (this.objects && this.objects.indexOf(obj) >= 0)
        {
            this.objects = this.objects.filter(ind => ind != obj);
            this.removeCount();
        }
    }

    add(obj, ignoreCount)
    {
        if (this.objects.indexOf(obj) < 0)
        {
            this.objects.push(obj);

            if (!ignoreCount)
                this.addCount();
        }
    }

    divide()
    {
        this.createNodes();
        let center = new vector2((this.x1 + this.x2) / 2, (this.y1 + this.y2) / 2);

        this.objects.forEach(object =>
        {
            if (object.position.y > center.y)
            {
                if (object.position.x > center.x)
                {
                    object.node = this.bottomright;
                    this.bottomright.add(object, true);
                }
                else
                {
                    object.node = this.bottomleft;
                    this.bottomleft.add(object, true);
                }
            }
            else
            {
                if (object.position.x > center.x)
                {
                    object.node = this.topright;
                    this.topright.add(object, true);
                }
                else
                {
                    object.node = this.topleft;
                    this.topleft.add(object, true);
                }
            }
        });

        this.objects = [];
    }

    merge()
    {
        this.getChildren().forEach(child =>
        {
            child.getObjects().forEach(object =>
            {
                this.add(object, true);
                object.node = this;
            })
        })

        this.removeNodes();
    }

    size()
    {
        if (this.objects != null)
        {
            return this.objects.length;
        }
        return 0;
    }

    getObjects()
    {
        return this.objects;
    }

    getChildren()
    {
        return [this.topleft, this.topright, this.bottomleft, this.bottomright];
    }

    findChildNode(position)
    {
        if (!this.hasChildren)
            return this;

        if (position.x >= this.topleft.x1 && position.x <= this.topleft.x2 && position.y >= this.topleft.y1 && position.y <= this.topleft.y2)
        {
            return this.topleft;
        }
        else if (position.x >= this.bottomleft.x1 && position.x <= this.bottomleft.x2 && position.y >= this.bottomleft.y1 && position.y <= this.bottomleft.y2)
        {
            return this.bottomleft;
        }
        else if (position.x >= this.topright.x1 && position.x <= this.topright.x2 && position.y >= this.topright.y1 && position.y <= this.topright.y2)
        {
            return this.topright;
        }
        else if (position.x >= this.bottomright.x1 && position.x <= this.bottomright.x2 && position.y >= this.bottomright.y1 && position.y <= this.bottomright.y2)
        {
            return this.bottomright;
        }
    }

    createNodes()
    {
        if (this.hasChildren)
            return;

        let depth = this.depth + 1;

        this.topleft = new Node(this.x1, this.y1, this.x1 + this.halfLengthX, this.y1 + this.halfLengthY, depth, this, this.name + 0, this.quadTree);

        this.topright = new Node(this.x1 + this.halfLengthX, this.y1, this.x2, this.y1 + this.halfLengthY, depth, this, this.name + 1, this.quadTree);

        this.bottomleft = new Node(this.x1, this.y1 + this.halfLengthY, this.x1 + this.halfLengthX, this.y2, depth, this, this.name + 2, this.quadTree);

        this.bottomright = new Node(this.x1 + this.halfLengthX, this.y1 + this.halfLengthY, this.x2, this.y2, depth, this, this.name + 3, this.quadTree);

        return this.getChildren();
    }

    createParent(nodePosition)
    {
        let parentNode;

        if (nodePosition == "topleft")
        {
            parentNode = new Node(this.x1, this.y1, this.lengthX * 2, this.lengthY * 2, this.depth - 1, "", this.quadTree);

            parentNode.topleft == this;
        }

        if (nodePosition == "topright")
        {
            parentNode = new Node(this.x1 - this.lengthX, this.y1, this.lengthX * 2, this.lengthY * 2, this.depth - 1, "", this.quadTree);

            parentNode.topright == this;
        }

        if (nodePosition == "bottomleft")
        {
            parentNode = new Node(this.x1, this.y1 - this.lengthY, this.lengthX * 2, this.lengthY * 2, this.depth - 1, "", this.quadTree);

            parentNode.bottomleft == this;
        }

        if (nodePosition == "bottomright")
        {
            parentNode = new Node(this.x1 - this.lengthX, this.y1 - this.lengthY, this.lengthX * 2, this.lengthY * 2, this.depth - 1, "", this.quadTree);

            parentNode.bottomright == this;
        }

        if (!parentNode.topleft)
        {
            parentNode.topleft = new Node(parentNode.x1, parentNode.y1, parentNode.x1 + parentNode.halfLengthX, parentNode.y1 + parentNode.halfLengthY, this.depth, parentNode, parentNode.name + 0, this.quadTree);
        }

        if (!parentNode.topright)
        {
            parentNode.topright = new Node(parentNode.x1 + parentNode.halfLengthX, parentNode.y1, parentNode.x2, parentNode.y1 + parentNode.halfLengthY, this.depth, parentNode, parentNode.name + 1, this.quadTree);
        }

        if (!parentNode.bottomleft)
        {
            parentNode.bottomleft = new Node(parentNode.x1, parentNode.y1 + parentNode.halfLengthY, parentNode.x1 + parentNode.halfLengthX, parentNode.y2, this.depth, parentNode, parentNode.name + 2, this.quadTree);
        }

        if (!parentNode.bottomright)
        {
            parentNode.bottomright = new Node(parentNode.x1 + parentNode.halfLengthX, parentNode.y1 + parentNode.halfLengthY, parentNode.x2, parentNode.y2, this.depth, parentNode, parentNode.name + 3, this.quadTree);
        }

        this.startReconfigureNames();
        return parentNode;
    }

    startReconfigureNames()
    {
        let parent = this;
        while (parent.parent)
        {
            parent = parent.parent;
        }

        parent.reconfigureNames("");
    }

    reconfigureNames(name)
    {
        this.name = name;

        for (let i = 0, l = this.getChildren().length; i < l; i++)
        {
            this.getChildren()[i].reconfigureNames(name + i);
        }
    }

    removeNodes()
    {
        this.topleft = null;
        this.topright = null;
        this.bottomleft = null;
        this.bottomright = null;
    }

    update(deltaTime)
    {
        if (this.hasChildren)
        {
            this.topleft.update(deltaTime);
            this.topright.update(deltaTime);
            this.bottomleft.update(deltaTime);
            this.bottomright.update(deltaTime);
        }
    }

    /**
     * Retrieves the same-level neighbour if possible, or the lowest level if not.
     * 
     * @param {string} direction L, R, U or D 
     */
    getNeighbour(direction)
    {
        let name = this.name.split("");
        let hmap = { 0: 1, 1: 0, 2: 3, 3: 2 }
        let vmap = { 0: 2, 2: 0, 1: 3, 3: 1 }

        switch (direction)
        {
            case "L":
                if (name[name.length - 1] == "0" || name[name.length - 1] == "2")
                {
                    if (name.length > 1)
                        name[name.length - 2] = hmap[name[name.length - 2]]
                    else
                        return null;
                }
                name[name.length - 1] = hmap[name[name.length - 1]]
                break;
            case "R":
                if (name[name.length - 1] == "1" || name[name.length - 1] == "3")
                {
                    if (name.length > 1)
                        name[name.length - 2] = hmap[name[name.length - 2]]
                    else
                        return null;
                }
                name[name.length - 1] = hmap[name[name.length - 1]]
                break;
            case "U":
                if (name[name.length - 1] == "0" || name[name.length - 1] == "1")
                {
                    if (name.length > 1)
                        name[name.length - 2] = vmap[name[name.length - 2]]
                    else
                        return null;
                }
                name[name.length - 1] = vmap[name[name.length - 1]]
                break;
            case "D":
                if (name[name.length - 1] == "2" || name[name.length - 1] == "3")
                {
                    if (name.length > 1)
                        name[name.length - 2] = vmap[name[name.length - 2]]
                    else
                        return null;
                }
                name[name.length - 1] = vmap[name[name.length - 1]]
                break;
        }

        return quadTree.traverseTo(name.join(""));
    }

    show(ctx)
    {
        ctx.strokeStyle = "white";
        ctx.strokeRect(this.x1, this.y1, this.lengthX, this.lengthY);
    }
}